﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace PROGAssignment4BrighamMoll.Business_Logic
{
    /// <summary>
    /// A zodiac for horoscopes.
    /// </summary>
    [DataContract]
    public class ZodiacSign
    {
        /// <summary>
        /// The name of this zodiac symbol.
        /// </summary>
        [DataMember(Name = "Name")]
        private string _name;

        /// <summary>
        /// The image of this zodiac symbol.
        /// </summary>
        private BitmapImage _symbol;

        /// <summary>
        /// The array of strings representing possible subjects in predictions.
        /// </summary>
        private string[] _arrSubjects;

        /// <summary>
        /// The array of strings representing possible verbs in predictions.
        /// </summary>
        private string[] _arrVerbs;

        /// <summary>
        /// Generates random numbers to generate new predictions.
        /// </summary>
        private Random _predictionRandomizer;

        /// <summary>
        /// Predictions generated on the first run of the app will be stored here.
        /// </summary>
        private string[] _storedPredictions;

        /// <summary>
        /// Constructor for a zodiac. Initializes name and symbol of zodiac, before loading
        /// possible verbs and subjects for predictions.
        /// </summary>
        /// <param name="name"> The name of this zodiac. </param>
        /// <param name="symbol"> The symbol of this zodiac. </param>
        /// <param name="subjects"> The possible subjects list for predictions. </param>
        /// <param name="verbs"> The possible verbs list for precitions. </param>
        /// <param name="predictionRandomizer"> The randomizer for generating new predictions. </param>
        public ZodiacSign(string name, BitmapImage symbol, string[] verbs, string[] subjects, Random predictionRandomizer)
        {
            // Initialize defaults from parameters.
            _name = name;
            _symbol = symbol;
            _arrSubjects = subjects;
            _arrVerbs = verbs;
            _predictionRandomizer = predictionRandomizer;

            // Create empty array for stored predictions.
            _storedPredictions = new string[30];

            // Generate predictions to be used while the app runs, and store them in a list.
            for(int predictionsStored = 0; predictionsStored < 30; predictionsStored++)
            {
                string newPrediction = GeneratePrediction();
                _storedPredictions[predictionsStored] = newPrediction;
            }
        }

        /// <summary>
        /// The image file for this zodiac's symbol.
        /// </summary>
        public BitmapImage SymbolImage
        {
            get { return _symbol; }
            set { _symbol = value; }
        }

        /// <summary>
        /// The name of this zodiac.
        /// </summary>
        public string ZodiacName
        {
            get { return _name; }
        }

        /// <summary>
        /// The array of prediction strings stored in this zodiac.
        /// </summary>
        public string[] PredictionsStored
        {
            set { _storedPredictions = value; }
            get { return _storedPredictions; }
        }

        /// <summary>
        /// Call this method to generate a prediction for a certain date for this zodiac.
        /// </summary>
        /// <param name="date"> The date the prediction is being made for. </param>
        /// <returns> The prediction string. </returns>
        public string GeneratePrediction()
        {
            // Choose a random subject and verb from the lists.
            string subject = _arrSubjects[_predictionRandomizer.Next(0,19)];
            string verb = _arrVerbs[_predictionRandomizer.Next(0, 19)];

            // Use these subjects and verbs to generate a prediction, and return it.
            string prediction = $"{subject} will {verb}.";
            return prediction;
        }
    }
}
