﻿using PROGAssignment4BrighamMoll.Data_Persistence;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace PROGAssignment4BrighamMoll.Business_Logic
{
    /// <summary>
    /// The horoscope class that stores all predictions and zodiac signs.
    /// </summary>
    class Horoscope
    {
        /// <summary>
        /// A list of all the zodiacs.
        /// </summary>
        private List<ZodiacSign> _zodiacList;

        /// <summary>
        /// The array of strings representing possible subjects in predictions.
        /// </summary>
        private string[] _arrSubjects;

        /// <summary>
        /// The array of strings representing possible verbs in predictions.
        /// </summary>
        private string[] _arrVerbs;

        /// <summary>
        /// The randomizer used to generate random predictions.
        /// </summary>
        private Random _predictionRandomizer;

        /// <summary>
        /// The serializer that saves both files for the application.
        /// </summary>
        private HoroscopePersistService _horoscopeSerializer;

        /// <summary>
        /// Constructor for the horoscope class, initializes zodiacs list.
        /// </summary>
        public Horoscope()
        {
            // Create an empty zodiac list.
            _zodiacList = new List<ZodiacSign>();

            // Initialize serializer.
            _horoscopeSerializer = new HoroscopePersistService(_zodiacList);

            // Create a randomizer.
            _predictionRandomizer = new Random();

            // Fill array of verbs for predictions.
            _arrVerbs = new string[20] { "sprain an ankle", "over-eat", "have breakfast with the president", "sleep too much", "drive to the mountains",
                "figure out the secret of a riddle", "fail a exam", "pray to a false god", "become a god", "find a bug in some code", "train in a forest",
                "learn a new language", "discover something once lost", "wake up from this dream called life", "find a cheeseburger under a couch",
                "find order in chaos", "trample your foes", "study far too much", "win a distorted prize", "find the key to believing" };

            // Fill array of subjects for predictions.
            _arrSubjects = new string[20] { "You", "Your brother", "Your sister", "Your friend", "Your father", "Your mother", "Your god", "Your pet",
                "Your grandparents", "Your worst enemy", "Your lover", "Your best friend", "Your long lost relative", "Your uncle", "Your aunt", "Your role-model",
                "Your forgotten samurai friend", "Your third cousin thrice removed", "Your alternate universe self", "Your pet lizard you didn't know you had" };

            // Fill the list with zodiacs.
            GenerateZodiacs();

            // Create the zodiac-prediction array, fill it, and store it in the horoscopeSerializer for saving purposes.
            string[,] predictionStore = new string[12, 30];
            // The first dimension will be the zodiac, while the second is each prediction.
            for(int zodiacNum = 0; zodiacNum < 12; zodiacNum++)
            {
                for (int predictionNum = 0; predictionNum < 30; predictionNum++)
                {
                    predictionStore[zodiacNum, predictionNum] = _zodiacList[zodiacNum].PredictionsStored[predictionNum];
                }
            }

            // Store the array in the serializer.
            _horoscopeSerializer.PredictionStore = predictionStore;
        }

        /// <summary>
        /// The randomizer that chooses random predictions.
        /// </summary>
        public Random PredictionRandomizer
        {
            get { return _predictionRandomizer; }
        }

        /// <summary>
        /// The serializer that saves the files in the horoscope app.
        /// </summary>
        public HoroscopePersistService HoroscopeSerializer
        {
            get { return _horoscopeSerializer; }
        }

        /// <summary>
        /// This method generates all zodiac objects and adds them to the list. (Also loads zodiac objects if saved, along with predictions.)
        /// </summary>
        public void GenerateZodiacs()
        {
            // Create an array of zodiac images to pass to the zodiac objects.
            BitmapImage[] zodiacImagesArray = new BitmapImage[12] { new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Aquarius.png")),
                new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Aries.png")), new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Cancer.png")),
                new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Capricorn.png")), new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Gemini.png")),
                new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Leo.png")), new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Libra.png")),
                new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Pisces.png")), new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Sagittarius.png")),
                new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Scorpio.png")), new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Taurus.png")),
                new BitmapImage(new Uri("ms-appx:///Assets/HoroscopeAssets/Virgo.png")) };

            // Try to load previously made predictions and zodiac signs from files.
            _horoscopeSerializer.LoadHoroscope();

            // If there is now a list of zodiac objects in the serializer, use those.
            if (_horoscopeSerializer.ZodiacList.Count != 0)
            {
                _zodiacList = _horoscopeSerializer.ZodiacList;

                // For each zodiac from the list, set its images and predictions.
                for (int zodiacNum = 0; zodiacNum < 12; zodiacNum++)
                {
                    _zodiacList[zodiacNum].SymbolImage = zodiacImagesArray[zodiacNum];
                    _zodiacList[zodiacNum].PredictionsStored = new string[30];
                    // Load predictions from saved ones in serializer.
                    for (int predictionNum = 0; predictionNum < 30; predictionNum++)
                    {
                        _zodiacList[zodiacNum].PredictionsStored[predictionNum] = _horoscopeSerializer.PredictionStore[zodiacNum, predictionNum];
                    }

                }
            }
            // If there are no imported zodiacs, create the default ones.
            else
            {
                // Create an array of zodiac names to pass to the zodiac objects.
                string[] zodiacNamesArray = new string[12] { "Aquarius", "Aries", "Cancer", "Capricorn", "Gemini", "Leo", "Libra", "Pisces", "Sagittarius", "Scorpio", "Taurus", "Virgo" };

                // Create 12 zodiac objects. (Assign a number to each for prediction calculations.
                // Then, add each to the list.
                for (int zodiacNum = 1; zodiacNum < 13; zodiacNum++)
                {
                    ZodiacSign newZodiac = new ZodiacSign(zodiacNamesArray[zodiacNum - 1], zodiacImagesArray[zodiacNum - 1], _arrVerbs, _arrSubjects, _predictionRandomizer);
                    _zodiacList.Add(newZodiac);
                }
            }
        }

        /// <summary>
        /// This method determines a user's zodiac and returns it based on their birthday.
        /// The index of the list is based on an alphabetical sorting of the zodiacs.
        /// </summary>
        /// <param name="userBirthday"> The user's birthday. </param>
        /// <returns> The zodiac sign that matches the user's birthday. </returns>
        public ZodiacSign DetermineUserZodiac(DateTimeOffset userBirthday)
        {
            // Make a case for each month of the year. Within the cases, determine signs.
            switch (userBirthday.Month)
            {
                case 1:
                    if (userBirthday.Day < 20) // The user is a Capricorn.
                    {  return _zodiacList[3]; }
                    else // The user is an Aquarius.
                    { return _zodiacList[0]; }

                case 2:
                    if (userBirthday.Day < 19) // The user is an Aquarius.
                    { return _zodiacList[0]; }
                    else // The user is a Pisces.
                    { return _zodiacList[7]; }

                case 3:
                    if (userBirthday.Day < 21) // The user is a Pisces.
                    { return _zodiacList[7]; }
                    else // The user is an Aries.
                    { return _zodiacList[1]; }

                case 4:
                    if (userBirthday.Day < 20) // The user is an Aries.
                    { return _zodiacList[1]; }
                    else // The user is a Taurus.
                    { return _zodiacList[10]; }

                case 5:
                    if (userBirthday.Day < 21) // The user is a Taurus.
                    { return _zodiacList[10]; }
                    else // The user is a Gemini.
                    { return _zodiacList[4]; }

                case 6:
                    if (userBirthday.Day < 21) // The user is a Gemini.
                    { return _zodiacList[4]; }
                    else // The user is a Cancer.
                    { return _zodiacList[2]; }

                case 7:
                    if (userBirthday.Day < 23) // The user is a Cancer.
                    { return _zodiacList[2]; }
                    else // The user is a Leo.
                    { return _zodiacList[5]; }

                case 8:
                    if (userBirthday.Day < 23) // The user is a Leo.
                    { return _zodiacList[5]; }
                    else // The user is a Virgo.
                    { return _zodiacList[11]; }

                case 9:
                    if (userBirthday.Day < 23) // The user is a Virgo.
                    { return _zodiacList[11]; }
                    else // The user is a Libra.
                    { return _zodiacList[6]; }

                case 10:
                    if (userBirthday.Day < 23) // The user is a Libra.
                    { return _zodiacList[6]; }
                    else // The user is a Scorpio.
                    { return _zodiacList[9]; }

                case 11:
                    if (userBirthday.Day < 22) // The user is a Scorpio.
                    { return _zodiacList[9]; }
                    else // The user is a Sagittarius.
                    { return _zodiacList[8]; }

                case 12:
                    if (userBirthday.Day < 22) // The user is a Sagittarius.
                    { return _zodiacList[8]; }
                    else // The user is an Capricorn.
                    { return _zodiacList[3]; }

                // Code should never arrive here.
                default:
                    Debug.Assert(false, "Error, invalid month.");
                    return null;
            }
        }
    }
}
