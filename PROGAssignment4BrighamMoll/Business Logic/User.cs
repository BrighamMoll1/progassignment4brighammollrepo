﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;

namespace PROGAssignment4BrighamMoll.Business_Logic
{
    /// <summary>
    /// The user who is using the app.
    /// </summary>
    class User
    {
        /// <summary>
        /// The name of the user using the app.
        /// </summary>
        private string _name;

        /// <summary>
        /// The birthday of the user using the app.
        /// </summary>
        private DateTimeOffset _birthday;

        /// <summary>
        /// The image control on the page containing the user's avatar.
        /// </summary>
        private Image _avatarControl;

        /// <summary>
        /// The actual image file for the avatar of this user.
        /// </summary>
        private BitmapImage _avatarImage;

        /// <summary>
        /// The constructor for the user, for when the app is launched. Initializes user values to default.
        /// Alos sets user's avatar image control to null and sets the default avatar image.
        /// </summary>
        public User()
        {
            // Initialize defaults and set image control for avatar.
            _name = "Joe Shmoe";
            _birthday = new DateTimeOffset(new DateTime(1988, 11, 14));
            _avatarControl = null;
            _avatarImage = new BitmapImage(new Uri("ms-appx:///Assets/AvatarDefault.png"));
        }

        /// <summary>
        /// The actual image file used for the user's avatar.
        /// </summary>
        public BitmapImage AvatarImage
        {
            get { return _avatarImage; }
            set { _avatarImage = value; }
        }

        /// <summary>
        /// The image control used to place the avatar image for a user.
        /// </summary>
        public Image AvatarControl
        {
            set { _avatarControl = value; }
        }

        /// <summary>
        /// The birthday of this user.
        /// </summary>
        public DateTimeOffset Birthday
        {
            get { return _birthday; }
            set { _birthday = value; }
        }

        /// <summary>
        /// The name of this user.
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
    }
}
