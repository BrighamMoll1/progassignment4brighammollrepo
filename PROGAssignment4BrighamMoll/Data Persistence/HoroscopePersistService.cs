﻿using PROGAssignment4BrighamMoll.Business_Logic;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;

namespace PROGAssignment4BrighamMoll.Data_Persistence
{
    /// <summary>
    /// This class is meant to store zodiacs and prediction information to actual files on the device.
    /// </summary>
    public class HoroscopePersistService
    {
        /// <summary>
        /// A two-dimensional array for storing all the predictions in the application by zodiac type.
        /// </summary>
        private string[,] _predictionStore;

        /// <summary>
        /// A list of all the zodiacs.
        /// </summary>
        private List<ZodiacSign> _zodiacList;

        /// <summary>
        /// The file path of the save files for the application.
        /// </summary>
        protected string _saveFileDirectoryPath;

        /// <summary>
        /// The constructor for the HoroscopePersistService data persistence object. Creates a folder to save/load the
        /// horoscope app files. (zodiac-sign-store.json, and prediction-store.dat)
        /// </summary>
        public HoroscopePersistService(List<ZodiacSign> zodiacList)
        {
            // Initialize _predictionStore as null, it will be changed through a property.
            _predictionStore = null;

            // Use parameter to fill the zodiac list.
            _zodiacList = zodiacList;

            // Determine the directory path of the save folder, where the two save files will be stored in.
            _saveFileDirectoryPath = Path.Combine(ApplicationData.Current.LocalFolder.Path, "HoroscopeData");

            // If the directory does not exist, create it. (If it exists already, nothing needs to be done.)
            if (Directory.Exists(_saveFileDirectoryPath) == false)
            {
                Directory.CreateDirectory(_saveFileDirectoryPath);
            }
        }

        /// <summary>
        /// The predictions array to be stored in a save file.
        /// </summary>
        public string[,] PredictionStore
        {
            get { return _predictionStore; }
            set { _predictionStore = value; }
        }

        /// <summary>
        /// A zodiac list imported from a save file. Null if not imported.
        /// </summary>
        public List<ZodiacSign> ZodiacList
        {
            get { return _zodiacList; }
        }

        /// <summary>
        /// Loads all stored prediction and zodiac data into the application.
        /// </summary>
        public void LoadHoroscope()
        {
            // Determine the zodiac save file name and prediction save file name.
            string zodiacSaveFile = $"{_saveFileDirectoryPath}/zodiac-sign-store.json";
            string predictionSaveFile = $"{_saveFileDirectoryPath}/prediction-store.dat";

            // Open the zodiac file if it exists, saving the objects from it to the serializer.
            if (File.Exists(zodiacSaveFile) == true)
            {
                using (FileStream acctStream = new FileStream(zodiacSaveFile, FileMode.Open))
                {
                    // Deserialize the zodiac sign object list.
                    DataContractJsonSerializer zodiacSerializer = new DataContractJsonSerializer(typeof(List<ZodiacSign>));
                    List<ZodiacSign> zodiacList = zodiacSerializer.ReadObject(acctStream) as List<ZodiacSign>;
                    _zodiacList = zodiacList;
                }
            }

            // Load the predictions stored, if the file exists.
            if (File.Exists(predictionSaveFile) == true)
            {
                using (StreamReader predictionReader = new StreamReader(new FileStream(predictionSaveFile, FileMode.Open)))
                {
                    // Create the zodiac-prediction array and fill it with saved information.
                    _predictionStore = new string[12, 30];
                    // The first dimension will be the zodiac, while the second is each prediction.
                    for (int zodiacNum = 0; zodiacNum < 12; zodiacNum++)
                    {
                        // Read the name of the zodiac.
                        predictionReader.ReadLine();
                        // Read each prediction for that zodiac, storing them in the array.
                        for (int predictionNum = 0; predictionNum < 30; predictionNum++)
                        {
                            _predictionStore[zodiacNum, predictionNum] = predictionReader.ReadLine();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Saves all stored prediction and zodiac data from the application.
        /// </summary>
        public void SaveHoroscope()
        {
            // Make zodiac-sign-store.json file to store all zodiac objects.

            // Determine file name.
            string zodiacFileName = $"{_saveFileDirectoryPath}/zodiac-sign-store.json";

            // Write the zodiac information to the file.
            using (FileStream zodiacStream = new FileStream(zodiacFileName, FileMode.Create))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<ZodiacSign>));

                serializer.WriteObject(zodiacStream, _zodiacList);
            }

            // Make prediction-store.dat file to store all predictions for the zodiacs.

            // Determine file name.
            string predictionStoreFileName = $"{_saveFileDirectoryPath}/prediction-store.dat";

            // Create the file and save the prediction information.
            using (StreamWriter predictionSaver = new StreamWriter(new FileStream(predictionStoreFileName, FileMode.Create)))
            {
                foreach(ZodiacSign zodiac in _zodiacList)
                {
                    // Write the name of the zodiac.
                    predictionSaver.WriteLine($"{zodiac.ZodiacName}");
                    // Write the predictions of that zodiac.
                    foreach(string prediction in zodiac.PredictionsStored)
                    {
                        predictionSaver.WriteLine(prediction);
                    }
                }
            }

        }
    }
}
