﻿using PROGAssignment4BrighamMoll.Business_Logic;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace PROGAssignment4BrighamMoll.Presentation
{
    /// <summary>
    /// The daily horoscope page.
    /// </summary>
    public sealed partial class DailyHoroscopePage : Page
    {
        /// <summary>
        /// The user of the application.
        /// </summary>
        private User _user;

        /// <summary>
        /// The horoscope class where all zodiacs are stored.
        /// </summary>
        private Horoscope _horoscope;

        /// <summary>
        /// The object passed to this page through navigation, containing the horoscope object and the edit profile pane.
        /// </summary>
        private PageNavStorage _pageNavStorage;

        /// <summary>
        /// Constructor for the daily horoscope page. Initializes and creates a user object.
        /// </summary>
        public DailyHoroscopePage()
        {
            this.InitializeComponent();

            // Initialize user object as null. It will be passed in on navigation.
            _user = null;

            // Initialize horoscope class object as null. It will be passed in on navigation.
            _horoscope = null;
        }

        /// <summary>
        /// Call this method to update the displayed horoscope images and information.
        /// </summary>
        public void UpdateHoroscope()
        {
            // Update the zodiac image to the user's, as well as the textblock displaying their zodiac name.
            ZodiacSign userZodiac = _horoscope.DetermineUserZodiac(_user.Birthday);
            _imgHoroscopeSymbol.Source = userZodiac.SymbolImage;
            _txtZodiac.Text = $"Zodiac: {userZodiac.ZodiacName}";

            // Update user avatar to proper avatar image.
            _imgAvatar.Source = _user.AvatarImage;

            // Update the name and birthday of this user displayed.
            _txtName.Text = $"Name: {_user.Name}";
            _txtBirthday.Text = $"Birthday: {_user.Birthday.Date.ToString("MMMM", CultureInfo.InvariantCulture)} {_user.Birthday.Day}, {_user.Birthday.Year}";

            // Update the prediction text with a new random prediction from the stored predictions in the zodiac.
            _txtHoroscopePrediction.Text = userZodiac.PredictionsStored[_horoscope.PredictionRandomizer.Next(0,29)];
        }

        /// <summary>
        /// When the daily horoscope page is navigated to when the app runs, it will
        /// take in the horoscope object for business logic.
        /// </summary>
        /// <param name="e"> The horoscope object being passed from the main page. </param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            // Take the passed in object and store the horoscope.
            _pageNavStorage = e.Parameter as PageNavStorage;
            _horoscope = _pageNavStorage.Horoscope;
            _user = _pageNavStorage.User;

            // Set the user's image control.
            _user.AvatarControl = _imgAvatar;

            // Display the proper information for the current user.
            UpdateHoroscope();
        }

        /// <summary>
        /// When the date is changed, the app will look for a new horoscope for the new day.
        /// </summary>
        /// <param name="sender"> The date picker. </param>
        /// <param name="e"> Arguments related to the date picker. </param>
        private void OnDateChanged(object sender, DatePickerValueChangedEventArgs e)
        {
            // Whenever the date is changed, the app will look for a new horoscope for the new day.
            UpdateHoroscope();
        }

        /// <summary>
        /// This method is called when the edit profile button is pressed to open the right pane.
        /// </summary>
        /// <param name="sender"> The edit profile button on the Daily Horoscope page. </param>
        /// <param name="e"> The arguments related to the button. </param>
        private void OnEditProfile(object sender, RoutedEventArgs e)
        {
            // Tell the main page to open the pane on the right side for profile editing.
            _pageNavStorage.PaneProfileEdit.IsPaneOpen = !_pageNavStorage.PaneProfileEdit.IsPaneOpen;

        }
    }
}
