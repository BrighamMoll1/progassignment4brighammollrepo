﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace PROGAssignment4BrighamMoll.Presentation
{
    /// <summary>
    /// Represents each navigation menu item.
    /// </summary>
    class NavMenuItem
    {
        /// <summary>
        /// Navigation menu item label. (Automatic property.)
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Navigation menu item symbol. (Automatic property.)
        /// </summary>
        public Symbol Symbol { get; set; }

        /// <summary>
        /// Used to access symbol as a character in XAML. (Automatic property.)
        /// </summary>
        public char SymbolAsChar
        {
             get { return (char)Symbol; }
        }

    }
}
