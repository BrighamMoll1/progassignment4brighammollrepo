﻿using PROGAssignment4BrighamMoll.Business_Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;

namespace PROGAssignment4BrighamMoll.Presentation
{
    /// <summary>
    /// This class exists to store the objects that need to be passed between pages during the operation of the app.
    /// This includes the _horoscope object with the business logic, and also the edit profile pane so that it can be opened
    /// from a button on the content frame despite being part of the outer navigation frame. The user object is also here so
    /// that information can be fetched from it for the edit profile pane and the daily horoscope page.
    /// </summary>
    class PageNavStorage
    {
        /// <summary>
        /// The horoscope object with the application's business logic.
        /// </summary>
        private Horoscope _horoscope;


        /// <summary>
        /// The splitview pane for profile editing that is shared between both the outer frame and the content frame through
        /// the edit profile button and the navigation menu option to edit the profile.
        /// </summary>
        private SplitView _paneProfileEdit;

        /// <summary>
        /// The user object that stores information related to the user.
        /// </summary>
        private User _user;

        /// <summary>
        /// The contructor for the PageNavStorage class, to be passed between pages while the application is running.
        /// </summary>
        /// <param name="horoscope"> The horoscope object containing the business logic for the application. </param>
        /// <param name="paneProfileEdit"> The pane for editing the user profile. </param>
        public PageNavStorage(Horoscope horoscope, SplitView paneProfileEdit, User user)
        {
            // Initialize variables with passed in parameters.
            _horoscope = horoscope;

            _paneProfileEdit = paneProfileEdit;

            _user = user;
        }

        /// <summary>
        /// The splitview pane that allows profile editing.
        /// </summary>
        public SplitView PaneProfileEdit
        {
            get { return _paneProfileEdit; }
            set { _paneProfileEdit = value; }
        }

        /// <summary>
        /// The horoscope object containing application business logic.
        /// </summary>
        public Horoscope Horoscope
        {
            get { return _horoscope; }
            set { _horoscope = value; }
        }

        /// <summary>
        /// The user object storing the information related to the user of the application.
        /// </summary>
        public User User
        {
            get { return _user; }
            set { _user = value; }
        }
    }
}
