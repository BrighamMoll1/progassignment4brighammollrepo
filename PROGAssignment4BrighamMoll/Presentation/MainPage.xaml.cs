﻿using PROGAssignment4BrighamMoll.Business_Logic;
using PROGAssignment4BrighamMoll.Presentation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace PROGAssignment4BrighamMoll
{
    /// <summary>
    /// The main page of the horoscope app.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        /// <summary>
        /// This string represents the file name for the avatar image file used by the user of this application.
        /// </summary>
        private const string USER_AVATAR_IMG_FILE = "UserAvatarImage";

        /// <summary>
        /// The horoscope object for holding the business logic of the application. (Besides user class.)
        /// </summary>
        private Horoscope _horoscope;

        /// <summary>
        /// An object used to store other objects for moving them all from page to page. (Horoscope object, and edit profile pane.)
        /// </summary>
        private PageNavStorage _pageNavStorage;

        /// <summary>
        /// A user object to store the user's information such as their avatar, birthday, and name.
        /// </summary>
        private User _user;

        /// <summary>
        /// The selected date by the user on the edit profile pane.
        /// </summary>
        private DateTimeOffset _selectedDate;

        /// <summary>
        /// Contructor for the main page of the horoscope app.
        /// </summary>
        public MainPage()
        {
            this.InitializeComponent();

            // Create an instance of the horoscope class to run the business logic (Besides user class.).
            _horoscope = new Horoscope();

            // Create a new user object to store user information.
            _user = new User();

            // Set the edit profile pane name, calendar date, and avatar to the user's.
            _txtNameEditBox.Text = _user.Name;
            _imgAvatarEdit.Source = _user.AvatarImage;
            _cldViewBirthdaySelect.SetDisplayDate(_user.Birthday);

            // Set the _selectedDate value to the user's birthday for initialization.
            _selectedDate = _user.Birthday;

            // Create new object to store the profile editing pane and the horoscope object for navigation.
            _pageNavStorage = new PageNavStorage(_horoscope, _paneProfileEdit, _user);

            // Use the inner frame to navigate to the daily horoscope page.
            // Pass the _horoscope object so that business logic can be accessed there.
            _frmDisplayedContents.Navigate(typeof(DailyHoroscopePage), _pageNavStorage);

            // Save the horoscope files.
            _horoscope.HoroscopeSerializer.SaveHoroscope();
        }

        /// <summary>
        /// This method is called whenever an item on the navigation menu is pressed.
        /// </summary>
        /// <param name="sender"> The navigation item clicked. </param>
        /// <param name="e"> Arguments related to the navigation item clicked. </param>
        private void OnNavItemClick(object sender, ItemClickEventArgs e)
        {
            // Fetch the item clicked on the navigation menu.
            NavMenuItem clickedMenuItem = e.ClickedItem as NavMenuItem;

            // If the daily horoscope button is pressed, go to that page in the inner frame.
            if (clickedMenuItem == _uiNavDailyHoroscope)
            {
                _frmDisplayedContents.Navigate(typeof(DailyHoroscopePage), _pageNavStorage);
            }
            // If the edit profile button is pressed, open or close the editing pane on the right.
            else if (clickedMenuItem == _uiNavEditProfile)
            {
                _paneProfileEdit.IsPaneOpen = !_paneProfileEdit.IsPaneOpen;
            }
        }

        /// <summary>
        /// This method is called when the "..." button is pressed to change the user's avatar on their profile.
        /// </summary>
        /// <param name="sender"> The "..." button on the edit profile pane. </param>
        /// <param name="e"> The arguments related to this button. </param>
        private async void OnChangeAvatar(object sender, RoutedEventArgs e)
        {
            // Create a file open picker for the user to pick out an image file to be used as their avatar.
            FileOpenPicker avatarPicker = new FileOpenPicker();

            // Start in the picture library.
            avatarPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;

            // Accept these file types. (jpg, jpeg, png, bmp)
            avatarPicker.FileTypeFilter.Add(".jpg");
            avatarPicker.FileTypeFilter.Add(".jpeg");
            avatarPicker.FileTypeFilter.Add(".png");
            avatarPicker.FileTypeFilter.Add(".bmp");

            StorageFile userAvatarImageFile = await avatarPicker.PickSingleFileAsync();

            // If a file was properly picked, store it in application file storage.
            if (userAvatarImageFile != null)
            {
                // Save avatar image file and replace anything already there.
                StorageFile avatarImageFile = await ApplicationData.Current.LocalFolder.CreateFileAsync(USER_AVATAR_IMG_FILE, CreationCollisionOption.ReplaceExisting);
                await userAvatarImageFile.CopyAndReplaceAsync(avatarImageFile);

                // Set the image file to be the user's new avatar.
                LoadAvatarFileToPane();
            }
        }

        /// <summary>
        /// Loads an avatar image file from storage before setting it to be the user's image.
        /// </summary>
        private async void LoadAvatarFileToPane()
        {
            try
            {
                // Load up the file where the avatar image is stored in.
                StorageFile userAvatarImageFile = await ApplicationData.Current.LocalFolder.GetFileAsync(USER_AVATAR_IMG_FILE);

                // Create a stream to read the data on the image file.
                using (IRandomAccessStream imgStream = await userAvatarImageFile.OpenAsync(FileAccessMode.Read))
                {
                    // Use the image found in the reading stream to change the edit profile pane avatar picture.
                    BitmapImage userImage = new BitmapImage();
                    userImage.SetSourceAsync(imgStream);
                    _imgAvatarEdit.Source = userImage;

                    // Also store avatar in user's object.
                    _user.AvatarImage = userImage;
                }
            }

            // If no avatar file is found:
            catch (FileNotFoundException)
            {
                // Take the default avatar image file and store it instead, locally.
                StorageFile defaultAvatarImageFile = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///Assets/AvatarDefault.png"));
                defaultAvatarImageFile.CopyAsync(ApplicationData.Current.LocalFolder, USER_AVATAR_IMG_FILE);

                // Change the edit profile pane picture to the default one that is stored. (Do not change the user's actual avatar when unsuccessful.)
                _imgAvatarEdit.Source = new BitmapImage(new Uri("ms-appx:///Assets/AvatarDefault.png"));
            }

            // If there is any other file error:
            catch (IOException)
            {
                // Change the edit profile pane picture to the default one. (Do not change the user's actual avatar when unsuccessful.)
                _imgAvatarEdit.Source = new BitmapImage(new Uri("ms-appx:///Assets/AvatarDefault.png"));
            }
        }

        /// <summary>
        /// This method is called when the "Apply" button is pressed to apply the changes to a user's profile and save them.
        /// </summary>
        /// <param name="sender"> The "Apply" button on the edit profile pane. </param>
        /// <param name="e"> The arguments related to this button. </param>
        private void OnApply(object sender, RoutedEventArgs e)
        {
            // Set the name entered to be the user's name, if it is not blank.
            if(!String.IsNullOrWhiteSpace(_txtNameEditBox.Text))
            {
                _user.Name = _txtNameEditBox.Text;
            }

            // Set the birthday selected to be the user's birthday.
            _user.Birthday = _selectedDate;

            // Navigate the content frame to the current page again to refresh it.
            Type pageType = _frmDisplayedContents.CurrentSourcePageType;
            _frmDisplayedContents.Navigate(pageType, _pageNavStorage);

        }

        /// <summary>
        /// This method is called when a user selects a date on the calendarview object in the edit profile pane.
        /// </summary>
        /// <param name="sender"> The calendarview object in the edit profile pane. </param>
        /// <param name="args"> Arguments related to the calendarview in the edit profile pane. </param>
        private void OnDateSelectChange(CalendarView sender, CalendarViewSelectedDatesChangedEventArgs args)
        {
            _selectedDate = args.AddedDates[0];
        }
    }
}
